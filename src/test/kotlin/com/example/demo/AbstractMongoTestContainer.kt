package com.example.demo

import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.env.MapPropertySource
import org.springframework.test.context.ContextConfiguration
import org.testcontainers.containers.GenericContainer
import org.testcontainers.lifecycle.Startables
import org.testcontainers.utility.DockerImageName


@ContextConfiguration(initializers = [AbstractMongoTestContainer.Initializer::class])
open class AbstractMongoTestContainer {
    internal class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
        override fun initialize(
            applicationContext: ConfigurableApplicationContext
        ) {
            startContainers()
            val environment = applicationContext.environment
            val testcontainers = MapPropertySource(
                "testcontainers",
                createConnectionConfiguration()
            )
            environment.propertySources.addFirst(testcontainers)
        }

        companion object {
            private var mongo = GenericContainer(DockerImageName.parse("mongo:5"))

            private fun startContainers() {
                println("[AbstractMongoTestContainer] startContainers.")
                mongo.env = mutableListOf(
                    "MONGO_INITDB_ROOT_USERNAME=admin",
                    "MONGO_INITDB_ROOT_PASSWORD=secret",
                    "MONGO_INITDB_DATABASE=admin",
                )
                mongo.withExposedPorts(27017)
                Startables.deepStart(mongo).join()
            }

            private fun createConnectionConfiguration(): Map<String, String> {
                println("[AbstractMongoTestContainer] createConnectionConfiguration. ${mongo.host} ${mongo.firstMappedPort}")
                return mapOf(
                    "spring.datasource.url" to "mongodb://localhost:${mongo.firstMappedPort}/admin",
                    "spring.datasource.username" to "admin",
                    "spring.datasource.password" to "secret",
                )
            }
        }
    }
}