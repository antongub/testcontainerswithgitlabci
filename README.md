# Problem
Gitlab-Ci Pipeline always failed where tests ran with TestContainers

## TLDR;
This Project served to test some Gitlab-Ci pipelines where I encountered a Problem that on a specific machines a job failed.
The job failed because it could not access an IP-Address (The Docker Host IP Address). 
Turned out the specific machine had a Firewall enabled which blocked the Ip-Address from being accessed.


## Details
### Requirements:
- Had a CI project-runner, not a shared-runner.
- Up-to-date docker version.
- Up-to-date gitlab-runner version.
- Up-to-date testcontainer version.
 
### Through various trials, exceptions looked like:   
- `Caused by: java.util.concurrent.CompletionException: java.lang.IllegalStateException: Could not connect to Ryuk at 172.17.0.1:32780`
- or `Caused by: java.lang.IllegalStateException: Could not connect to Ryuk at host.docker.internal:32777`
- or `Caused by: java.lang.IllegalStateException: Could not find a valid Docker environment. Please see logs and check configuration`
- or `Caused by: org.testcontainers.containers.ContainerLaunchException: Timed out waiting for container port to open (172.17.0.1 ports: [32784] should be listening)`

### Problems through Process
- Local installed runner worked but the runner registered on a debian server did not. config.toml was identical.
- Various sources said "host.docker.internal" as a domain name does not work on linux (only windows and mac supported) by default.
  - setting a custom /etc/hosts entry to "host.docker.internal" didn't work either.
- Disabling Ryuk didn't work.
- Tried setting Runner (and Ryuk) privilege to true.
- Firstly I thought it was a Problem with Ryuk, the TestContainer Resource Manager. Turns out later it was a general Problem accessing the Docker Host IP.
- In the end I tried setting up a Raspberry pi with the same setup as the debian server.
  - The raspberry pi setup worked.

## Solution
1. The Problem was a Firewall on the Debian Server which prevented access to the `172.17.0.1` IP Address.
  - `sudo ufw disable` fixed the Job in the Short-Term.
2. See [.gitlab-ci.yml](.gitlab-ci.yml)	
3. Gitlab-ci Project-Runner config.toml:
```toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "XXX"
  url = "https://gitlab.com/"
  id = "XXX"
  token = "XXX"
  token_obtained_at = "XXX"
  token_expires_at = "XXX"
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0

```

## Useful links
- https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3251
- https://java.testcontainers.org/features/configuration/#customizing-ryuk-resource-reaper
- https://java.testcontainers.org/supported_docker_environment/continuous_integration/gitlab_ci/
- https://stackoverflow.com/questions/46916796/connect-to-docker-daemon-from-inside-docker-container